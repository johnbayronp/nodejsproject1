$(".carousel").carousel({
    interval: 3000
});

$('#compra').on('show.bs.modal', function(e){
    $('#btnBuy').addClass('btn btn-danger');
    $('#btnBuy').prop('disabled',true);
});

$('#compra').on('hide.bs.modal', function(e){

    $('#btnBuy').removeClass('btn btn-danger');
    $('#btnBuy').addClass('btn btn-primary');
    $('#btnBuy').prop('disabled',false);
})
